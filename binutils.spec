%undefine __brp_strip_static_archive
%undefine _strict_symbol_defs_build

Summary: A GNU collection of binary utilities
Name: binutils
Version: 2.41
Release: 13%{?dist}
License: GPLv3+
URL: https://sourceware.org/binutils
Source0: https://ftp.gnu.org/gnu/binutils/binutils-%{version}.tar.xz
Patch0001: fixed-CVE-2024-53589.patch
Patch0002: https://github.com/RTEMS/sourceware-mirror-binutils-gdb/commit/baac6c221e9d69335bf41366a1c7d87d8ab2f893.patch
Patch3000: binutils-no-config-h-check.patch
Patch3001: binutils-revert-PLT-elision.patch
Patch3002: binutils-gold-warn-unsupported.patch
Patch3003: binutils-export-demangle.h.patch
Patch3004: binutils-gold-empty-dwp.patch
#Patch3005: binutils-gcc-10-fixes.patch
Patch3006: binutils-demangler-updates.patch
Patch3007: binutils-big-merge.patch
Patch3008: binutils-execstack-error.patch
Patch3009: binutils-gold-ignore-execstack-error.patch
Patch3010: binutils-gold-mismatched-section-flags.patch
Patch3011: binutils-gold-pack-relative-relocs.patch
Patch3012: binutils-handle-corrupt-version-info.patch
Patch3013: binutils-libtool-lib64.patch
Patch3014: binutils-libtool-no-rpath.patch
Patch3015: binutils-special-sections-in-groups.patch
Patch3016: binutils-ppc-dt_relr-relocs.patch
Patch3017: binutils-aarch64-big-bti-programs.patch
Patch3018: binutils-riscv-SUB_ULEB128.patch
Patch3019: binutils-x86-64-v3.patch
Patch3020: binutils-BPF-reloc-4.patch
Patch3021: LoongArch-Fix-ld-no-relax-bug.patch
Patch3022: LoongArch-Directly-delete-relaxed-instuctions-in-fir.patch
Patch3023: LoongArch-Multiple-relax_trip-in-one-relax_pass.patch
Patch3024: LoongArch-Remove-elf_seg_map-info-output_bfd-NULL-re.patch
Patch3025: LoongArch-Modify-link_info.relax_pass-from-3-to-2.patch
Patch3026: LoongArch-Add-more-relaxation-testcases.patch
Patch3027: LoongArch-fix-internal-error-when-as-handling-unsupp.patch
Patch3028: as-Add-new-atomic-instructions-in-LoongArch-v1.1.patch
Patch3029: as-Add-new-estimated-reciprocal-instructions-in-Loon.patch
Patch3030: LoongArch-Modify-inconsistent-behavior-of-ld-with-un.patch
Patch3031: backport-ld-Remove-JANSSON_LIBS-from-ld_new_DEPENDENCIES.patch
Patch3032: LoongArch-Fix-immediate-overflow-check-bug.patch
Patch3033: LoongArch-ld-Simplify-inserting-IRELATIVE-relocation.patch
Patch3034: Libvtv-Add-loongarch-support.patch
Patch3035: LoongArch-implement-count_-leading-trailing-_zeros.patch
Patch3036: LoongArch-gas-Fix-make-check-gas-crash.patch
Patch3037: Use-32-64_PCREL-to-replace-a-pair-of-ADD32-64-and-SU.patch
Patch3038: Add-testcase-for-generation-of-32-64_PCREL.patch
Patch3039: Make-sure-DW_CFA_advance_loc4-is-in-the-same-frag.patch
Patch3040: LoongArch-Enable-gas-sort-relocs.patch
Patch3041: Add-support-for-pcaddi-rd-symbol.patch
Patch3042: as-add-option-for-generate-R_LARCH_32-64_PCREL.patch
Patch3043: Add-testsuits-for-new-assembler-option-of-mthin-add-.patch
Patch3044: LoongArch-GAS-Add-support-for-branch-relaxation.patch
Patch3045: LoongArch-readelf-d-RELASZ-excludes-.rela.plt-size.patch
Patch3046: LoongArch-Correct-comments.patch
Patch3047: as-fixed-internal-error-when-immediate-value-of-relo.patch
Patch3048: Add-support-for-ilp32-register-alias.patch
Patch3049: MIPS-GAS-Add-march-loongson2f-to-loongson-2f-3-test.patch
Patch3050: LoongArch-Add-support-for-b-.L1-and-beq-t0-t1-.L1.patch
Patch3051: LoongArch-Add-new-relocation-R_LARCH_CALL36.patch
Patch3052: LoongArch-Add-call36-and-tail36-pseudo-instructions-.patch
Patch3053: LoongArch-Allow-la.got-la.pcrel-relaxation-for-share.patch
Patch3054: LoongArch-Add-support-for-the-third-expression-of-.a.patch
Patch3055: Re-LoongArch-Add-support-for-b-.L1-and-beq-t0-t1-.L1.patch
Patch3056: LoongArch-Add-new-relocs-and-macro-for-TLSDESC.patch
Patch3057: LoongArch-Add-support-for-TLSDESC-in-ld.patch
Patch3058: LoongArch-Add-tls-transition-support.patch
Patch3059: LoongArch-Add-support-for-TLS-LD-GD-DESC-relaxation.patch
Patch3060: LoongArch-Add-testsuit-for-DESC-and-tls-transition-a.patch
Patch3061: asan-buffer-overflow-in-loongarch_elf_rtype_to_howto.patch
Patch3062: LoongArch-bfd-Add-support-for-tls-le-relax.patch
Patch3063: LoongArch-include-Add-support-for-tls-le-relax.patch
Patch3064: LoongArch-opcodes-Add-support-for-tls-le-relax.patch
Patch3065: LoongArch-gas-Add-support-for-tls-le-relax.patch
Patch3066: LoongArch-ld-Add-support-for-tls-le-relax.patch
Patch3067: LoongArch-Commas-inside-double-quotes.patch
Patch3068: LoongArch-Fix-some-macro-that-cannot-be-expanded-pro.patch
Patch3069: LoongArch-Fix-loongarch-elf-target-ld-testsuite-fail.patch
Patch3070: LoongArch-Fix-linker-generate-PLT-entry-for-data-sym.patch
Patch3071: loongarch-index-shadows-global.patch
Patch3072: LoongArch-Discard-extra-spaces-in-objdump-output.patch
Patch3073: LoongArch-ld-Adjusted-some-code-order-in-relax.exp.patch
Patch3074: LoongArch-Fix-relaxation-overflow-caused-by-section-.patch
Patch3075: LoongArch-Adapt-R_LARCH_-PCALA-GOT-TLS_IE-TLS_DESC-6.patch
Patch3076: LoongArch-Do-not-emit-R_LARCH_RELAX-for-two-register.patch
Patch3077: LoongArch-Use-tab-to-indent-assembly-in-TLSDESC-test.patch
Patch3078: LoongArch-Do-not-add-DF_STATIC_TLS-for-TLS-LE.patch
Patch3079: LoongArch-Fix-some-test-failures-about-TLS-desc-and-.patch
Patch3080: LoongArch-gas-Don-t-define-LoongArch-.align.patch
Patch3081: LoongArch-gas-Start-a-new-frag-after-instructions-th.patch
Patch3082: LoongArch-ld-Add-support-for-TLS-LE-symbol-with-adde.patch
Patch3083: LoongArch-gas-Add-support-for-s9-register.patch
Patch3084: LoongArch-Fix-a-bug-of-getting-relocation-type.patch
Patch3085: LoongArch-gas-Fix-the-types-of-symbols-referred-with.patch
Patch3086: LoongArch-gas-Try-to-avoid-R_LARCH_ALIGN-associate-w.patch
Patch3087: LoongArch-bfd-Correct-the-name-of-R_LARCH_SOP_POP_32.patch
Patch3088: LoongArch-bfd-Fix-some-bugs-of-howto-table.patch
Patch3089: LoongArch-ld-Fix-other-pop-relocs-overflow-check-and.patch
Patch3090: Avoid-unused-space-in-.rela.dyn-if-sec-was-discarded.patch
Patch3091: LoongArch-Run-overflow-testcases-only-on-LoongArch-t.patch
Patch3092: LoongArch-Add-gas-testsuit-for-alias-instructions.patch
Patch3093: LoongArch-Add-gas-testsuit-for-lbt-lvz-instructions.patch
Patch3094: LoongArch-Add-gas-testsuit-for-lsx-lasx-instructions.patch
Patch3095: LoongArch-Add-gas-testsuit-for-LA64-int-float-instru.patch
Patch3096: LoongArch-Add-gas-testsuit-for-LA32-int-float-instru.patch
Patch3097: LoongArch-Add-gas-testsuit-for-LA64-relocations.patch
Patch3098: LoongArch-Add-gas-testsuit-for-LA32-relocations.patch
Patch3099: LoongArch-Delete-extra-instructions-when-TLS-type-tr.patch
Patch3100: LoongArch-Add-dtpoff-calculation-function.patch
Patch3101: LoongArch-Fix-some-test-cases-for-TLS-transition-and.patch
Patch3102: LoongArch-Fix-gas-and-ld-test-cases.patch
Patch3103: LoongArch-Scan-all-illegal-operand-instructions-with.patch
Patch3104: LoongArch-Add-relaxation-for-R_LARCH_CALL36.patch
Patch3105: BFD-Fix-the-bug-of-R_LARCH_AGLIN-caused-by-discard-s.patch
Patch3106: LoongArch-gas-Ignore-.align-if-it-is-at-the-start-of.patch
Patch3107: LoongArch-Fix-the-issue-of-excessive-relocation-gene.patch
Patch3108: LoongArch-ld-Move-.got-.got.plt-before-.data-and-pro.patch
Patch3109: LoongArch-ld-Report-an-error-when-seeing-an-unrecogn.patch
Patch3110: LoongArch-Add-mignore-start-align-option.patch
Patch3111: LoongArch-The-symbol-got-type-can-only-be-obtained-a.patch
Patch3112: LoongArch-Add-bad-static-relocation-check-and-output.patch
Patch3113: LoongArch-gas-Simplify-relocations-in-sections-witho.patch
Patch3114: Fix-building-Loongarch-BFD-with-a-32-bit-compiler.patch
Patch3115: LoongArch-Fix-ld-test-failures-caused-by-using-instr.patch
Patch3116: LoongArch-gas-Adjust-DWARF-CIE-alignment-factors.patch
Patch3117: Re-LoongArch-gas-Adjust-DWARF-CIE-alignment-factors.patch
Patch3118: LoongArch-Fix-relaxation-overflow-caused-by-ld-z-sep.patch
Patch3119: LoongArch-Make-align-symbol-be-in-same-section-with-.patch
Patch3120: LoongArch-Disable-linker-relaxation-if-set-the-addre.patch
Patch3121: LoongArch-add-.option-directive.patch
Patch3122: LoongArch-TLS-IE-needs-only-one-dynamic-reloc.patch
Patch3123: LoongArch-Do-not-check-R_LARCH_SOP_PUSH_ABSOLUTE-to-.patch
Patch3124: LoongArch-Remove-unused-code-in-ld-test-suite.patch
Patch3125: LoongArch-Reject-R_LARCH_32-from-becoming-a-runtime-.patch
Patch3126: LoongArch-Fix-bad-reloc-with-mixed-visibility-ifunc-.patch
Patch3127: LoongArch-Make-protected-function-symbols-local-for-.patch
Patch3128: LoongArch-Add-DT_RELR-support.patch
Patch3129: LoongArch-Add-DT_RELR-tests.patch
Patch3130: LoongArch-Not-alloc-dynamic-relocs-if-symbol-is-abso.patch
Patch3131: LoongArch-Fix-dwarf3-test-cases-from-XPASS-to-PASS.patch
Patch3132: LoongArch-Fix-ld-FAIL-test-cases.patch
Patch3133: LoongArch-Add-support-for-OUTPUT_FORMAT-binary.patch
Patch3134: loongarch-ld-testsuite-xpasses.patch
Patch3135: LoongArch-Fix-assertion-failure-with-DT_RELR.patch
Patch3136: LoongArch-Fix-DT_RELR-and-relaxation-interaction.patch
Patch3137: LoongArch-Fix-wrong-relocation-handling-of-symbols-d.patch
Patch3138: LoongArch-LoongArch64-allows-relocations-to-use-64-b.patch
Patch3139: LoongArch-Fixed-ABI-v1.00-TLS-dynamic-relocation-gen.patch
Patch3140: Add-macros-to-get-opcode-of-instructions-approriatel.patch
Patch3141: Not-append-rela-for-absolute-symbol.patch
Patch3142: LoongArch-Add-elfNN_loongarch_mkobject-to-initialize.patch
Patch3143: LoongArch-Fixed-R_LARCH_-32-64-_PCREL-generation-bug.patch
Patch3144: LoongArch-Optimize-the-relaxation-process.patch
Patch3145: LoongArch-Add-more-relaxation-support-for-call36.patch
Patch3146: LoongArch-Force-relocation-for-every-reference-to-th.patch
Patch3147: LoongArch-Fixed-precedence-of-expression-operators-i.patch
Patch3148: Include-ldlex.h-when-compile-eelfxxloongarch.c.patch
Patch3149: Modify-test-because-of-readelf-not-update.patch
Patch3150: remove-file-produced-by-bison.patch
Patch3151: replace-space-with-tab.patch
Patch3152: LoongArch-binutils-compatible-with-older-gcc.diff
Patch3153: Fix-slowdown-about-partial-linking.patch

BuildRequires: automake, autoconf, make
BuildRequires: perl, sed, coreutils, chrpath
BuildRequires: gettext, flex, zlib-devel, texinfo >= 4.0, /usr/bin/pod2man
BuildRequires: dejagnu, zlib-static, glibc-static, sharutils, bc, libstdc++
Requires(post): %{_sbindir}/alternatives
Requires(preun): %{_sbindir}/alternatives
Requires(post): coreutils
Provides: bundled(libiberty)

# Loongarch needs yyac
BuildRequires: bison

%ifnarch riscv64 loongarch64
%bcond_without gold
%else
%bcond_with gold
%endif

%ifnarch x86_64 aarch64
%bcond_with gprofng
#%undefine gprofng
%else
%bcond_without gprofng
%endif

%if "%toolchain" == "clang"
%bcond_without clang
%else
%bcond_with clang
%endif

%if %{with gold}
Requires: binutils-gold >= %{version}
%endif

%if %{with clang}
BuildRequires: clang compiler-rt
%else
BuildRequires: gcc
%endif

%description
The GNU Binutils are a collection of binary tools. The main ones are
ld, as and gold.
But they also include:
addr2line - Converts addresses into filenames and line numbers.
ar - A utility for creating, modifying and extracting from archives.
c++filt - Filter to demangle encoded C++ symbols.
dlltool - Creates files for building and using DLLs.
elfedit - Allows alteration of ELF format files.
gprof - Displays profiling information.
nlmconv - Converts object code into an NLM.
nm - Lists symbols from object files.
objcopy - Copies and translates object files.
objdump - Displays information from object files.
ranlib - Generates an index to the contents of an archive.
readelf - Displays information from any ELF format object file.
size - Lists the section sizes of an object or archive file.
strings - Lists printable strings from files.
strip - Discards symbols.
windmc - A Windows compatible message compiler.
windres - A compiler for Windows resource files.

%if %{with gprofng}
%package gprofng
Summary: Next Generating code profiling tool
Requires: binutils = %{version}-%{release}
Provides: gprofng = %{version}-%{release}
 
%description gprofng
GprofNG is the GNU Next Generation Profiler for analyzing the performance 
of Linux applications.
%endif

%package devel
Summary: BFD and opcodes static and dynamic libraries and header files
Requires: zlib-devel, coreutils, binutils = %{version}-%{release}
Provides: binutils-static = %{version}-%{release}

%description devel
This package provides static and dynamic libraries of BFD and opcodes.

%if %{with gold}
%package gold
Summary: The GOLD linker, a faster alternative to the BFD linker
BuildRequires: bison, m4, gcc-c++, libstdc++-static
Requires: binutils >= %{version}
Provides: gold = %{version}-%{release}

%description gold
This package provides the GOLD linker, which is intended to have
complete support for ELF. The GOLD is generally faster than the
older GNU linker.
%endif

%package source
Summary: source code included %{_vendor} patch
BuildRequires: tar, xz
BuildArch: noarch

%description source
This package provides source code included %{_vendor} patch
for cross toolchains



%prep
%autosetup -p1

sed -i -e '/#define.*ELF_COMMONPAGESIZE/s/0x1000$/0x10000/' bfd/elf*ppc.c
sed -i -e '/#define.*ELF_COMMONPAGESIZE/s/0x1000$/0x10000/' bfd/elf*aarch64.c
sed -i -e '/common_pagesize/s/4 /64 /' gold/powerpc.cc
sed -i -e '/pagesize/s/0x1000,/0x10000,/' gold/aarch64.cc
find . -name *.texi -print -exec touch {} \;

# take tarball of source code
tar acvf %{name}-%{version}-%{release}.tar.xz *



%build
%if %{with clang}
%global enable_lto 0
%global _lto_cflags %{nil}
%else
%global enable_lto 1
%endif

export LDFLAGS="$RPM_LD_FLAGS"

case %{_target_platform} in
    ppc64le*)
        CARGS=" --enable-targets=powerpc-linux,spu,x86_64-pep,bpf-unknown-none"
        ;;
    *)
        CARGS=" --enable-64-bit-bfd --enable-targets=x86_64-pep,bpf-unknown-none"
        ;;
esac

%configure \
  --quiet \
  --build=%{_target_platform} \
  --host=%{_target_platform} \
  --target=%{_target_platform} \
%if %{with gold}
  --enable-gold=default \
%endif	
%if %{with gprofng}
  --enable-gprofng=yes \
%else
  --enable-gprofng=no \
%endif
  --enable-ld \
  --with-sysroot=/ \
  --with-system-zlib \
  --enable-deterministic-archives=no \
  --enable-compressed-debug-sections=none \
  --enable-generate-build-notes=no \
  --enable-separate-code=yes \
  --enable-shared \
%if %{enable_lto}
  --enable-lto \
%endif
  --enable-new-dtags \
  --disable-rpath \
  --enable-threads=yes \
  --enable-relro=yes \
  $CARGS \
  --enable-plugins

%make_build tooldir=%{_prefix} all
%make_build tooldir=%{_prefix} info

%install
%make_install

# Rebuild libiberty.a with -fPIC.
# Future: Remove it together with its header file, projects should bundle it.
%make_build -C libiberty clean
%set_build_flags
%make_build CFLAGS="-g -fPIC $CFLAGS" -C libiberty

install -m 644 libiberty/libiberty.a %{buildroot}%{_libdir}
install -m 644 include/libiberty.h %{buildroot}%{_prefix}/include

chmod +x %{buildroot}%{_libdir}/lib*.so*
rm -f %{buildroot}%{_mandir}/man1/{dlltool,nlmconv,windres,windmc}*
rm -f %{buildroot}%{_libdir}/lib{bfd,opcodes}.so
rm -f %{buildroot}%{_libdir}/*.la

%ifarch x86_64
OP_FMT='elf64-x86-64'
%endif

%ifarch aarch64
OP_FMT='elf64-littleaarch64'
%endif

%ifarch riscv64
OP_FMT='elf64-littleriscv'
%endif

%ifarch loongarch64
OP_FMT='elf64-loongarch64'
%endif

tee %{buildroot}%{_libdir}/libbfd.so <<EOH
/* GNU ld script */

OUTPUT_FORMAT($OP_FMT)

INPUT ( %{_libdir}/libbfd.a %{_libdir}/libsframe.a -liberty -lz -ldl )
EOH

tee %{buildroot}%{_libdir}/libopcodes.so <<EOH
/* GNU ld script */

OUTPUT_FORMAT($OP_FMT)

INPUT ( %{_libdir}/libopcodes.a -lbfd )
EOH

rm -f %{buildroot}%{_infodir}/dir
rm -rf %{buildroot}%{_prefix}/%{_target_platform}

%find_lang binutils
%find_lang opcodes
%find_lang bfd
%find_lang gas
%find_lang gprof
cat opcodes.lang >> binutils.lang
cat bfd.lang >> binutils.lang
cat gas.lang >> binutils.lang
cat gprof.lang >> binutils.lang

if [ -x ld/ld-new ]; then
  %find_lang ld
  cat ld.lang >> binutils.lang
fi
if [ -x gold/ld-new ]; then
  %find_lang gold
  cat gold.lang >> binutils.lang
fi

cd %{buildroot}%{_bindir}
chrpath --delete $(find ./ -type f -exec file {} \; | grep "\<ELF\>" | awk -F ':' '{print $1 }' | tr '\n' ' ')
cd -
chrpath --delete %{buildroot}%{_libdir}/libopcodes-%{version}.so
chrpath --delete %{buildroot}%{_libdir}/libbfd-%{version}.so
chrpath --delete %{buildroot}%{_libdir}/libctf.so.0.0.0
%if %{with gprofng}
chrpath --delete  %{buildroot}%{_libdir}/libgprofng.so.0.0.0
%endif

# copy source code tarball to installing directory
mkdir -p %{buildroot}%{_usrsrc}/%{name}
cp -f %{name}-%{version}-%{release}.tar.xz  %{buildroot}%{_usrsrc}/%{name}/



%check
make -k check || :



%post
rm -f %{_bindir}/ld
%{_sbindir}/alternatives --install %{_bindir}/ld ld \
  %{_bindir}/ld.bfd 50

%if %{with gold}
%{_sbindir}/alternatives --install %{_bindir}/ld ld \
  %{_bindir}/ld.gold 30
%endif
exit 0

%preun
if [ $1 = 0 ]; then
  %{_sbindir}/alternatives --remove ld %{_bindir}/ld.bfd
fi
%if %{with gold}
if [ $1 = 0 ]; then
  %{_sbindir}/alternatives --remove ld %{_bindir}/ld.gold
fi
%endif
exit 0


%files -f binutils.lang
%license COPYING COPYING3 COPYING3.LIB COPYING.LIB
%doc README
%{_bindir}/*
%{_libdir}/lib*.so.*
%{_libdir}/bfd-plugins/libdep.so
%{_libdir}/libbfd-%{version}.so
%{_libdir}/libctf-nobfd.so
%{_libdir}/libctf.so
%{_libdir}/libsframe.so
%{_libdir}/libopcodes-%{version}.so
%exclude %{_bindir}/ld.gold
%exclude %{_libdir}/libbfd.so
%exclude %{_libdir}/libopcodes.so
%{_infodir}/*
%{_mandir}/man1/
%if %{with gprofng}
%{_libdir}/libgprofng.so
%exclude %{_bindir}/gp-*
%exclude %{_bindir}/gprofng
%exclude %{_infodir}/gprofng*
%exclude %{_mandir}/man1/gp-*
%exclude %{_mandir}/man1/gprofng*
%endif

%if %{with gprofng}
%files gprofng
%{_bindir}/gp-*
%{_bindir}/gprofng
%{_mandir}/man1/gp-*
%{_mandir}/man1/gprofng*
%{_infodir}/gprofng.info.*
%dir %{_libdir}/gprofng
%{_libdir}/gprofng/*
%{_sysconfdir}/gprofng.rc
%endif

%files devel
%{_prefix}/include/*
%{_libdir}/lib*.a
%{_libdir}/libbfd.so
%{_libdir}/libopcodes.so

%if %{with gold}
%files gold
%{_bindir}/ld.gold
%endif

%files source
%{_usrsrc}/%{name}/%{name}-%{version}-%{release}.tar.xz



%changelog
* Thu Feb 20 2025 Tracker Robot <trackbot@opencloudos.tech> - 2.41-13
- [Type] bugfix
- [DESC] Apply patches from rpm-tracker
- [Bug Fix] baac6c221e9d69335bf41366a1c7d87d8ab2f893.patch: PR32560 stack-buffer-overflow at objdump disassemble_bytes

* Mon Jan 20 2025 Xin Wang <wangxin03@loongson.cn>  - 2.41-12
- [Type] other
- [DESC] Fix slowdown about partial linking

* Mon Dec 09 2024 Zhao Zhen <jeremiazhao@tencent.com> - 2.41-11
- [Type] security
- [DESC] fixed CVE-2024-53589

* Fri Nov 29 2024 Peng Fan <fanpeng@loongson.cn>  - 2.41-10
- LoongArch: keep compatible with older gcc.

* Sat Nov 16 2024 Xin Wang <wangxin03@loongson.cn> - 2.41-9
- [Type] other
- [DESC] LoongArch: update release subversion

* Thu Nov 07 2024 Xin Wang <wangxin03@loongson.cn> - 2.41-8
- [Type] other
- [DESC] LoongArch: sync patch from binutils upstream

* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.41-7
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.41-6
- Rebuilt for loongarch release

* Mon Jun 24 2024 Zhao Zhen <jeremiazhao@tencent.com> - 2.41-5
- provides a new subpackage included patched source for cross toolchains

* Fri May 24 2024 Huang Yang <huangyang@loongson.cn> - 2.41-4
- [Type] bugfix
- [DESC] binutils requires yacc to compile on the loongarch

* Wed Mar 13 2024 Peng Fan <fanpeng@loongson.cn> - 2.41-3
- LoongArch: sync patch from binutils upstream

* Thu Feb 01 2024 Miaojun Dong <zoedong@tencent.com> - 2.41-2
- Accept and ignore R_BPF_64_NODYLD32 relocations

* Mon Jan 22 2024 Zhao Zhen <jeremiazhao@tencent.com> - 2.41-1
- upgrade to upstream 2.41

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.40-2
- Rebuilt for OpenCloudOS Stream 23.09

* Wed Aug 02 2023 cunshunxia <cunshunxia@tencent.com> - 2.40-1
- Update to 2.40.

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.38-4
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.38-3
- Rebuilt for OpenCloudOS Stream 23

* Mon Jan 16 2023 cunshunxia <cunshunxia@tencent.com> - 2.38-2
- fix duplicate file in different pkgs.

* Thu Jul 28 2022 cunshunxia <cunshunxia@tencent.com> - 2.38-1
- initial build
